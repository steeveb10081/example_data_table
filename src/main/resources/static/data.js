$(document).ready(function() {

    var activityFilter = {
        startDate :"20210302",
        patient :{
            nom : "Blanchard",
            prenom : "Richard Steeve"
        }
    };

    $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "test",
            "type": "POST",
            "contentType": 'application/json',
            "data": function ( d ) {
                d.activityFilter = activityFilter;
                return JSON.stringify( d );
            }
        },
        "columns": [
            { "data": "nom" },
            { "data": "prenom" }
        ]
    } );
} );
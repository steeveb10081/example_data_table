package com.mbds.bf.bouspam.service;

import com.mbds.bf.bouspam.dao.BeneficiaireRepository;
import com.mbds.bf.bouspam.dto.DataRetour;
import com.mbds.bf.bouspam.entity.Beneficiaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeneficiaireService {
    @Autowired
   private BeneficiaireRepository beneficiaireRepository ;

    public  Page<Beneficiaire> findAllBeneficiaire(Integer pageNo, Integer pageSize){
        Pageable paging = PageRequest.of(pageNo, pageSize);
       Page<Beneficiaire> beneficiairePage = beneficiaireRepository.findAll(paging);
        return beneficiairePage;
    }
}

package com.mbds.bf.bouspam.dto;

import java.io.Serializable;

public class Enter implements Serializable {

    private Integer draw;
    private String nom;

    public Enter() {

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    @Override
    public String toString() {
        return "Enter{" +
                "nom='" + nom + '\'' +
                '}';
    }
}

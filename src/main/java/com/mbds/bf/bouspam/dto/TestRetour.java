package com.mbds.bf.bouspam.dto;

import com.mbds.bf.bouspam.entity.Beneficiaire;

import java.io.Serializable;
import java.util.List;

public class TestRetour implements Serializable {

    private Integer draw;
    private Long recordsTotal;
    private Integer recordsFiltered;
    private List<Beneficiaire> data;

    public TestRetour() {
    }

    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    public Long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(Long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public Integer getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(Integer recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<Beneficiaire> getData() {
        return data;
    }

    public void setData(List<Beneficiaire> data) {
        this.data = data;
    }
}

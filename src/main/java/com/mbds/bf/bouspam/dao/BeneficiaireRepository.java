package com.mbds.bf.bouspam.dao;

import com.mbds.bf.bouspam.entity.Beneficiaire;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BeneficiaireRepository extends PagingAndSortingRepository<Beneficiaire, Integer> {
    Page<Beneficiaire> findAll(Pageable pageable);
}

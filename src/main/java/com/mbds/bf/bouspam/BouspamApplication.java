package com.mbds.bf.bouspam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BouspamApplication {

    public static void main(String[] args) {
        SpringApplication.run(BouspamApplication.class, args);
    }

}

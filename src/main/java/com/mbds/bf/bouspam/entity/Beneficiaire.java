package com.mbds.bf.bouspam.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class Beneficiaire {

    private String idClient;
    @NotNull
    private String nom;
    private String prenom;
    private String telephone;
    private String email;

    @Id
    @Column(name = "id_client")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "telephone")
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Beneficiaire beneficiaire = (Beneficiaire) o;
        return Objects.equals(idClient, beneficiaire.idClient) &&
                Objects.equals(nom, beneficiaire.nom) &&
                Objects.equals(prenom, beneficiaire.prenom);
    }

}
